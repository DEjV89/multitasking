package pl.dejv.multitasking;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by Dawid Krężel on 31/05/16.
 */
public class Flow2Activity extends AppCompatActivity {

	private static final String TAG = "TAG";

	@Override
	protected void onCreate(@Nullable Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.flow_2);
		ButterKnife.bind(this);
		ActionBar actionBar = getSupportActionBar();
		if (actionBar != null) {
			actionBar.setTitle("Flow 2");
		}
		Log.i(TAG, this.toString());
	}

	@OnClick(R.id.flow2)
	public void onFlowClick() {
		setResult(RESULT_OK, getIntent());
		finish();
	}

	@Override
	public void onBackPressed() {
	}
}
