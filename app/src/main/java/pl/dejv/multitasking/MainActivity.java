package pl.dejv.multitasking;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.Button;
import android.widget.Toast;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class MainActivity extends AppCompatActivity {

	public static final int FLOW_CODE = 1;
	private static final String TAG = "TAG";

	@Bind(R.id.flow)
	protected Button mFlowBtn;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		ButterKnife.bind(this);
		Log.i(TAG, this.toString());
	}

	@OnClick(R.id.flow)
	public void onFlowClick() {
		Intent intent = new Intent(this, Flow1Activity.class);
		intent.putExtra("result", "No Siema");
		startActivityForResult(intent, FLOW_CODE);
	}

	@Override
	protected void onResume() {
		Log.i(TAG, this.toString());
		super.onResume();
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		if (requestCode == FLOW_CODE) {
			if (resultCode == RESULT_OK) {
				String res = data.getStringExtra("result");
				Toast.makeText(MainActivity.this, "Result passed back! : " + res, Toast.LENGTH_SHORT).show();
			}
		} else {
			super.onActivityResult(requestCode, resultCode, data);
		}
	}
}
