package pl.dejv.multitasking;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by Dawid Krężel on 31/05/16.
 */
public class Flow1Activity extends AppCompatActivity {

	private static final String TAG = "TAG";
	
	@Override
	protected void onCreate(@Nullable Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.flow_1);
		ButterKnife.bind(this);
		ActionBar actionBar = getSupportActionBar();
		if (actionBar != null) {
			actionBar.setTitle("Flow 1");
		}
		Log.i(TAG, this.toString());
	}

	@OnClick(R.id.flow1)
	public void onNextFlow() {
		Intent intent = getIntent();
		intent.setClass(this, Flow2Activity.class);
		intent.setFlags(Intent.FLAG_ACTIVITY_FORWARD_RESULT);
		startActivity(intent);
	}

	@Override
	public void onBackPressed() {
	}
}
